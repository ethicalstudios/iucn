(function ($) {
  Drupal.behaviors.ThemeIUCN = {
    userTabsMoved: false,
    attach: function (context, settings) {
      this.addHomeClasses();
      this.moveUserTabs();

      $(window).resize(function () {
        Drupal.behaviors.ThemeIUCN.moveUserTabs();
      });
    },
    //add some classes to the main nav if we're on the homepage
    addHomeClasses: function(){
      $('body.front #main-menu li.first').addClass('active-trail');
    },
    //combine the main-menu and secondary menu if the burger is showing (ie it's a small screen size)
    moveUserTabs: function(){
      if($('#user-menu').length){
        var navBarDisplay = $('#header').find('button.navbar-toggle').css('display');
        if(!this.userTabsMoved && navBarDisplay == 'none'){
          $('#user-menu').insertAfter('#main-menu');
          this.userTabsMoved = true;
        }
        else if(this.userTabsMoved && (navBarDisplay == 'block' || navBarDisplay == 'inline-block')){
          $('#user-menu').insertAfter('#secondary-menu');
          this.userTabsMoved = false;
        }
      }
    },
  }
})(jQuery);
