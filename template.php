<?php
/**
 * @file
 * Theme functions
 */

$theme_path = drupal_get_path('theme', 'iucn');

/*
 * switching the carousel arrows
 */
function iucn_preprocess_field(&$vars){
  if($vars['element']['#field_type'] == 'panopoly_spotlight'){
    if(!empty($vars['items'][0]['#markup'])){
      $vars['items'][0]['#markup'] = str_replace('icon-chevron-right', 'icon-angle-right', $vars['items'][0]['#markup']);
      $vars['items'][0]['#markup'] = str_replace('icon-chevron-left', 'icon-angle-left', $vars['items'][0]['#markup']);
    }
  }
}
